#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline author:t c:nil creator:comment d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t timestamp:t toc:nil todo:t |:t
#+TITLE: Easy Exams
#+AUTHOR: Jérémy Barbay
#+EMAIL: jeremy@barbay.cl
#+DESCRIPTION: Tools to facilitate the design and creation of controls and exams, including marking schemes, complete solutions, etc.
#+KEYWORDS: latex sty problem exam
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.4.1 (Org mode 8.2.5h)
#+OPTIONS: texht:t
#+DATE: \today
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS:
#+LATEX_HEADER:
#+LATEX_HEADER_EXTRA: \usepackage{fullpage}

#+begin_latex
\begin{abstract}
#+end_latex
Aiming to separate the distinct acts of coaching learners and of evaluating the knowledge of those learners,
we propose a serie of tools to help with the generation of controls and exams, which can later be used for the automation of those tasks.
Those tools include in particular style files for the writing of problems and exam in a modular way, so that to facilitate the creation and maintenance of a database of solved problems with suggestions of marking schemes.
#+begin_latex
\end{abstract}
#+end_latex

* Descriptions

The project can be described at various levels of details and to various public. 
You choose which description corresponds the best to your attention span and time available :)

** Elevator pitch(es):

A set of tools so that, given a short style file describing the course your teaching (in terms of its name, code, instructors), a short latex file specifying which problems you wish to include in your exam, automatically generates as printable files 1) the exam itself with space for answers, 2) a copy of the exam with complete solutions and marking schemes, to be given to the teaching assistants who will correct the exam, and 3) the exam with complete solutions, to be given to students as a pedagogical material after the marking.

Tools to help reduce the teaching load of academics!
https://www.instagram.com/p/BjuFztbnUGg/?utm_source=ig_share_sheet&igshid=1m3a88zg84c4m

** For Guides (aka Teachers)

While most academic institutions require equal share of teaching and research, in reality academics spend on average 59% of their time on Teaching and 18% on Research (and 23% on Services) [Higher Education Research Institute Survey 1999, via PhD Comics 2008]. We propose tools to ease the generation of assignment and exams via international collaboration on the design and reuse of solved problems, associated with their marking schemes.

*** TODO DESCRIBE the need for the separation of Teaching and Evaluation


** For Learners
** For Evaluators
** For Institutions (such as Schools and Universities)

* Milestones and Sub-Projects
** Cleanup =exam.cls=, =assignment.cls=, =problem.sty= for easier use by other users
** COMPLETE =exam.cls= so that the list of problems on the front page is generated automatically (as a table of content, only in array form)
** ADD \index commands to problems in order to create an index of problems by  learning objective (mayor and minor)
** Setup a script which, given a list of folders and an integer n, creates a random exam made of n problems.
* Funding
** Ideas of sources of funding
   - Universities?
** Ideas of business model
*** Boot Strapping Database
    - Randomly generated exams in pdf
      - for free (without solution nor marking scheme)
      - from validated schemes
    - LaTeX sources, Marking Scheme and Complete Solutions sent in exchange of 
      - *Time* via the 
       	- WRITING of new material (problems, marking schemes and complete solutions), which give "credit" when validated later or
       	- VALIDATING material (several solved problems); or simply 
      - *Money*, via Paypal payment.
* Resources required
 * A web server
 * A website with accounts to keep preferences such as 
   - content of course.sty
   - learning outcomes
 * A paypal account
